﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HelloWPFApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (txtName.Text != "" && txtName.Text != "<YourName>") { 
            if (RadioButton1.IsChecked == true)
            {
                MessageBox.Show("Hello, " + txtName.Text + ", have fun coding today!");
            }
            else
            {
                RadioButton2.IsChecked = true;
                MessageBox.Show("Goodbye, " + txtName.Text + ", drink more coffee next time!");
            }
        }else {
            MessageBox.Show("Please enter your name first...");
            txtName.Text = "<YourName>";
            txtName.SelectAll();
            txtName.Focus();

        }
        }
    }
}
